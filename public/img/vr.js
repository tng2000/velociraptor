String.prototype.format = String.prototype.f = function() {
    var s = this,
        i = arguments.length;

    while (i--) {
        s = s.replace(new RegExp('\\{' + i + '\\}', 'gm'), arguments[i]);
    }
    return s;
};

$(function() {

  var rw = $('.raptor').width();
  var solve1 = function() {
    $('.results').text("");
    //var xv = -40.0; //this is the initial location of velociraptor (you can change)
    var xh =  0; //location of human
    // var av =  4; //acceleration of velociraptor (you can change)
    // var ah =  3; //accel of human (can change)
    // var vvmax=25; //maximum velocity of the velociraptor (change)
    // var vhmax=6; //max velocity of human (change)
    var vh = 0; //starting velocity of human
    var vv = 0; //starting velocity of velociraptor
    var t  = 0; //starting time
    var dt = 0.1 //time step (you can play with this)

    var xv = 0 - parseInt( $('.xv').val() );
    var av = parseInt( $('.av').val() );
    var ah = parseInt( $('.ah').val() );
    var vhmax = parseInt( $('.vhmax').val() );
    var vvmax = parseInt( $('.vvmax').val() );

    console.log("xv: {0}".format(xv));

    var loop = function() {
      //first check if the human is at max v
      if (vh >= vhmax) {
        ah=0 //if yes, set accel to zero
      }
      //check if velociraptor is at max speed
      if (vv >= vvmax) {
        av=0; //if yes, set accel to zero
      }
      //calc new human velocity after time interval
      vh += ah * dt;

      //calc new velociraptor velocity
      vv += av * dt;

      //calc new positions
      xh += vh * dt;
      xv += vv * dt;

      //update time
      t = t + dt;

      $('.results').append( "{0}: Raptor: distance: {0}m, speed: {1} m/s\n".format(t.toFixed(1), (xh - xv).toFixed(2), vv.toFixed(2)) );
      $('.time-result').val( "{0}s".format(t.toFixed(2)) );

      $('.human').css('left', '{0}px'.format( (xh * 10) + 400));
      $('.raptor').css('left', '{0}px'.format((xv * 10) + 400 - rw));

      if (xv <= xh) {
        setTimeout( loop, 100);
      } else {
        $('#start-button').show();
      }
    };

    $('#start-button').hide();
    setTimeout( loop, 100);
  };

$('#start-button').click(solve1);
  console.log( "ready!" );
});